package main

import (
	"context"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	httpServer "gitlab.com/dobermangood/simple-mongo-server/internal/controller/http"
	jobTask "gitlab.com/dobermangood/simple-mongo-server/internal/controller/job/task"
	memoryRepoTask "gitlab.com/dobermangood/simple-mongo-server/internal/infrastructure/repo/memory/task"
	mongoRepoTask "gitlab.com/dobermangood/simple-mongo-server/internal/infrastructure/repo/mongo/task"
	useCaseTask "gitlab.com/dobermangood/simple-mongo-server/internal/usecase/task"
	clientMongo "gitlab.com/dobermangood/simple-mongo-server/pkg/mongo"
	"golang.org/x/sync/errgroup"
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())
	mongoClient, err := clientMongo.New(ctx)
	if err != nil {
		log.Fatal(err)
	}

	collection := mongoClient.Database("tasker").Collection("tasks")
	taskMongoRepo := mongoRepoTask.New(collection)
	taskMemoryRepo := memoryRepoTask.New()
	taskUseCase := useCaseTask.New(taskMongoRepo, taskMemoryRepo)
	taskJob := jobTask.New(5*time.Second, taskUseCase) // TODO: вынести в конфиг

	server := httpServer.New()
	errGroup, errCtx := errgroup.WithContext(ctx)

	errGroup.Go(func() error {
		return taskJob.Run(errCtx)
	})
	errGroup.Go(func() error {
		return server.Run(taskUseCase)
	})

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM, syscall.SIGINT)

	oscall := <-c
	cancel()

	server.Stop(ctx)
	mongoClient.Disconnect(ctx)
	errGroup.Wait()

	log.Print("shutdown: ", oscall)
}
