package mongo

import (
	"context"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func New(ctx context.Context) (*mongo.Client, error) {
	// TODO: вынести отдельно в конфиг
	clientOptions := options.Client().ApplyURI("mongodb://root:example@localhost:27017/")

	client, err := mongo.Connect(ctx, clientOptions)
	if err != nil {
		return nil, err
	}

	err = client.Ping(ctx, nil)
	if err != nil {
		return nil, err
	}

	return client, nil
}
