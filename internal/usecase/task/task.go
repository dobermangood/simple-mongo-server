package task

import (
	"context"

	"gitlab.com/dobermangood/simple-mongo-server/internal/entity"
)

type MongoRepo interface {
	GetList(ctx context.Context) ([]entity.Task, error)
	Insert(ctx context.Context, task entity.Task) error
	Delete(ctx context.Context, taskID string) error
}

type MemoryRepo interface {
	Save([]entity.Task) error
}

type UseCase struct {
	mongoRepo  MongoRepo
	memoryRepo MemoryRepo
}

func New(mongoRepo MongoRepo, memoryRepo MemoryRepo) *UseCase {
	return &UseCase{
		mongoRepo:  mongoRepo,
		memoryRepo: memoryRepo,
	}
}

func (uc *UseCase) GetList(ctx context.Context) ([]entity.Task, error) {
	tasks, err := uc.mongoRepo.GetList(ctx)
	return tasks, err
}

func (uc *UseCase) Insert(ctx context.Context, task entity.Task) error {
	err := uc.mongoRepo.Insert(ctx, task)
	return err
}

func (uc *UseCase) Delete(ctx context.Context, taskID string) error {
	err := uc.mongoRepo.Delete(ctx, taskID)
	return err
}

func (uc *UseCase) UpdateTasksInMemory(ctx context.Context) error {
	tasks, err := uc.mongoRepo.GetList(ctx)
	if err != nil {
		return err
	}

	err = uc.memoryRepo.Save(tasks)
	return err
}
