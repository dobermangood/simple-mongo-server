package task

import (
	"context"
	"errors"
	"fmt"

	"gitlab.com/dobermangood/simple-mongo-server/internal/entity"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type Repo struct {
	collection *mongo.Collection
}

func New(collection *mongo.Collection) *Repo {
	return &Repo{
		collection: collection,
	}
}

func (r *Repo) GetList(ctx context.Context) ([]entity.Task, error) {
	var tasks []entity.Task
	filter := bson.D{{}}

	cur, err := r.collection.Find(ctx, filter)
	if err != nil {
		return tasks, err
	}

	for cur.Next(ctx) {
		var t entity.Task
		err := cur.Decode(&t)
		if err != nil {
			return tasks, err
		}

		tasks = append(tasks, t)
	}

	if err := cur.Err(); err != nil {
		return tasks, err
	}

	cur.Close(ctx)
	return tasks, nil
}

func (r *Repo) Insert(ctx context.Context, task entity.Task) error {
	doc := bson.D{{"name", task.Name}}
	res, err := r.collection.InsertOne(context.TODO(), doc)
	id, ok := res.InsertedID.(primitive.ObjectID)

	if !ok {
		fmt.Println(res.InsertedID)
		return errors.New("invalid id")
	}

	task.ID = id.String()
	return err
}

func (r *Repo) Delete(ctx context.Context, taskID string) error {
	id, err := primitive.ObjectIDFromHex(taskID)
	if err != nil {
		return err
	}

	filter := bson.M{"_id": id}
	_, err = r.collection.DeleteOne(context.TODO(), filter)
	return err
}
