package task

import (
	"fmt"
	"sync"

	"gitlab.com/dobermangood/simple-mongo-server/internal/entity"
)

type Repo struct {
	mu    sync.Mutex
	tasks []entity.Task
}

func New() *Repo {
	return &Repo{}
}

func (r *Repo) Save(tasks []entity.Task) error {
	r.mu.Lock()
	defer r.mu.Unlock()
	r.tasks = tasks
	fmt.Println(r.tasks)
	return nil
}
