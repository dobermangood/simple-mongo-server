package task

import (
	"context"
	"time"
)

type UseCase interface {
	UpdateTasksInMemory(ctx context.Context) error
}

type Job struct {
	timeout time.Duration
	useCase UseCase
}

func New(timeout time.Duration, useCase UseCase) *Job {
	return &Job{
		timeout: timeout,
		useCase: useCase,
	}
}

func (j *Job) Run(ctx context.Context) error {
	ticker := time.NewTicker(j.timeout)
	for {
		select {
		case <-ctx.Done():
			return ctx.Err()
		case <-ticker.C:
			err := j.useCase.UpdateTasksInMemory(ctx)
			if err != nil {
				return err
			}
		}
	}
}
