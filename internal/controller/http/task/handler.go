package task

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/go-chi/chi/v5"
	"gitlab.com/dobermangood/simple-mongo-server/internal/entity"
)

type UseCase interface {
	GetList(ctx context.Context) ([]entity.Task, error)
	Insert(ctx context.Context, task entity.Task) error
	Delete(ctx context.Context, taskID string) error
}

type Handler struct {
	useCase UseCase
}

func New(useCase UseCase) *Handler {
	return &Handler{
		useCase: useCase,
	}
}

func (ph *Handler) GetList(w http.ResponseWriter, r *http.Request) {
	tasks, err := ph.useCase.GetList(r.Context())
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Println(err)
		return
	}

	jsonResponse, err := json.Marshal(tasks)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(jsonResponse)
}

func (ph *Handler) Delete(w http.ResponseWriter, r *http.Request) {
	taskID := chi.URLParam(r, "id")

	err := ph.useCase.Delete(r.Context(), taskID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Println(err)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (ph *Handler) Insert(w http.ResponseWriter, r *http.Request) {
	task := entity.Task{}
	err := json.NewDecoder(r.Body).Decode(&task)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	err = ph.useCase.Insert(r.Context(), task)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Println(err)
		return
	}

	w.WriteHeader(http.StatusOK)
}
