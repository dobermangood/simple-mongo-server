package http

import (
	"context"
	"net/http"

	"github.com/go-chi/chi/v5"
	"gitlab.com/dobermangood/simple-mongo-server/internal/controller/http/task"
)

type Server struct {
	server *http.Server
}

func New() *Server {
	return &Server{
		server: &http.Server{
			Addr: ":3000",
		},
	}
}

func (s *Server) Run(useCase task.UseCase) error {
	r := chi.NewRouter()

	taskHandler := task.New(useCase)
	r.Get("/tasks", taskHandler.GetList)
	r.Delete("/tasks/{id}", taskHandler.Delete)
	r.Post("/tasks", taskHandler.Insert)

	s.server.Handler = r
	return s.server.ListenAndServe()
}

func (s *Server) Stop(ctx context.Context) error {
	return s.server.Shutdown(ctx)
}
